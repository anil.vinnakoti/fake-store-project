import React, { Component } from 'react';
import axios from 'axios';
import Loading from './loading';
import Store from './store';


class Load extends Component {
    state = {
        itemsData: '',
        item :{
            title: '',
            description: '',
            price:0,
            image:'',
            category: ''
        }
      }


    addHandler = (event) =>{
        event.preventDefault();
        // console.log('Add clicked');
        const inputData = this.state

        axios
            .post('https://fakestoreapi.com/products', inputData)
            .then(res =>{
                const item = res.data
                item['rating'] = {};
                item['rating']['rate'] = '0';
                item['rating']['count'] = '0'
                const items = [...this.state.itemsData, item]
                this.setState({
                    itemsData:items
                })
                // console.log(items);
            })
            .catch(err => console.log(err))
    }

    inputHandler = (event) => {
        // console.log(event.target.dataset.input);
        this.setState({
            
            [event.target.dataset.input]: event.target.value
        })
    }

      fetchItems(){
        axios
            .get('https://fakestoreapi.com/products')
            .then(response => {
                const data = response.data;
                this.setState({
                    itemsData:data
                })
            })
            .catch(err => {
                this.setState({
                    itemsData:'error'
                })
            })
      }
    
      componentDidMount() {
        this.fetchItems()
        }

    render() { 
        const {itemsData} = this.state;
        return (itemsData === 'error') 
            ?(<div>Error occureed while loading data</div>)
            :(itemsData === '')
            ?(<Loading />)
            :(<Store 
                item ={this.state.item} 
                itemsData = {this.state.itemsData}
                inputHandler = {this.inputHandler}
                addHandler= {this.addHandler} />)
        
    }
}
 
export default Load;
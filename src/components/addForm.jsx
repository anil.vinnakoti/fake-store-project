import React, { Component } from 'react';
import AddButton from './addButton'

class AddForm extends Component {

    render() { 
        return (
            <form className='add-item' id='form'><h3>Add Item</h3>
                <div className="form-group row">
                    
                    <div className="col-sm-4">
                    <input data-input='title' onChange={this.props.inputHandler} type="text" className="form-control" placeholder="Title" />
                    </div>
                </div>
                <div className="form-group row">
                    
                    <div className="col-sm-4">
                    <input data-input='category' onChange={this.props.inputHandler} type="text" className="form-control" placeholder="Category" />
                    </div>
                </div> 
                <div className="form-group row">
                   
                    <div className="col-sm-4">
                    <input data-input='description' onChange={this.props.inputHandler} type="text" className="form-control" placeholder="Description" />
                    </div>
                </div>
                <div className="form-group row">
                    
                    <div className="col-sm-4">
                    <input data-input='image' onChange={this.props.inputHandler} type="text" className="form-control" placeholder="Image url" />
                    </div>
                </div> 
                <div className="form-group row">
                    
                    <div className="col-sm-4">
                    <input data-input='price' onChange={this.props.inputHandler} type="text" className="form-control" placeholder="Price" />
                    </div>
                </div>
 
               
                <AddButton add={this.props.add}/>

            </form>
        );
    }
}
 
export default AddForm;
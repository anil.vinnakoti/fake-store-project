import React, { Component } from 'react';

import Block from './block';
import Navbar from './navbar';
import Footer from './footer';
import AddForm from './addForm';

class Store extends Component {

    render() { 
        const {itemsData} = this.props;
        return ( 
            <>
                <Navbar />
                <div className='main'>
                    {itemsData.map((item) => 
                    <Block key = {item.id} itemData={this.props.itemsData} item={item}/>
                )}
                </div>
                <AddForm inputHandler = {this.props.inputHandler} add = {this.props.addHandler}/>
                <div>
                    <Footer />
                </div>

            </>
        );
    }
}
 
export default Store;
import React, { Component } from 'react';

class Footer extends Component {

    render() { 
        return (
          <div className='footer'>
            <div>
              <h3>Fake Store</h3>
            </div>

            <div>
              <p><a href='#'>Contact us</a></p>
              <p><a href='#'>FAQ'S</a></p>
              <p><a href='#'>Returns &amp; Exchanges</a></p>
              <p><a href='#'>Size Guide</a></p>
            </div>

            <div>
              <p><a href='#'>About us</a></p>
              <p><a href='#'>Collections</a></p>
            </div>

          </div>
        );
    }
}
 
export default Footer;